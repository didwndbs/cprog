#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int i,len;
    char str[128];
    scanf("%s", str);
    for(len=0; str[len]!='\0'; len++);
    for(i=len-1; i>=0; i--)
    {
        printf("%c",str[i]);
    }
    printf("\n");
    if(str[0]==str[len-1])
        printf("palindrome\n");
    else
        return 0;
}
