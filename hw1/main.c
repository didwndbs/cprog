#include <stdio.h>

void triR(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");

  // ...
  // 이 함수를 완성하시오. (4점)
  // ...
    int i, j, k, q;
    for (k = 1; k <= repeat; k++) // 반복
    {
        printf("\n");
        for (i = 1; i <= size; i++)
        {
            for (j = 1; j <= i; j++) // for문 반복 출력
                printf("%d", i); // 숫자 출력
            printf("\n");
            if(i==size)
            {
                for(q=size-1;q>=1;q--)
                {
                    for(j=0;j<q;j++)
                        printf("%d",q);
                        printf("\n");
                }
            }

        }


    }
    printf("Bye world\n");
}

void triL(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");

  // ...
  // 이 함수를 완성하시오. (1점)
  // ...

	printf("Bye world\n");
}

void dias(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");

  // ...
  // 이 함수를 완성하시오. (1점)
  // ...

	printf("Bye world\n");
}

int main(void)
{
  int n;
  scanf("%d", &n); // 1,2,3 중 하나를 입력받는다
  switch (n)
  {
    case 1: triR(); break;
    case 2: triL(); break;
    case 3: dias(); break;
    default: return -1;
  }
  return 0;
}
